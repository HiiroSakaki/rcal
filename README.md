# RCAL

EPITA IMAGE S9 RCAL class project
Done by Amélie Bertin and Thibault Buatois

## Setting up the environment

We use a conda environment for the project.
Please run these commands:

```
conda env create -f env.yml
conda activate rcal
```

## Running the project

With the environment ready, you can run :

```
python segment_visualize.py {PATH}
```

with `PATH` being the path to the MetaImage that you want to segment.

## Visualisation

The visualisation is a simple 3D Trackball Camera Interactor, with the brain
bounds in blue and the tumor in red. You can click to rotate the view. You can
use the mouse wheel to zoom in/out.

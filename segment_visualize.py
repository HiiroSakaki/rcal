import sys

######################## ITK ###############################
import itk

def segment_tumor(path):

    # Define types
    pixType = itk.F
    imgType = itk.Image[pixType, 3]

    # Load data
    img = itk.imread(path, pixType)

    # Segmentation with GAD filter
    segment_filter = itk.GradientAnisotropicDiffusionImageFilter[imgType,
                                                                 imgType].New(
            Input=img,
            NumberOfIterations=20,
            TimeStep=0.04,
            ConductanceParameter=3)

    # Thresholding with ConnectedThreshold filter
    threshold_filter = itk.ConnectedThresholdImageFilter[imgType, imgType].New()
    threshold_filter.SetReplaceValue(32767) # Short Max Value
    threshold_filter.SetLower(1000.0)
    threshold_filter.SetUpper(10000.0)
    threshold_filter.SetSeed((70, 80, 100))
    threshold_filter.SetInput(segment_filter.GetOutput())

    # Writing data for VTK
    itk.imwrite(threshold_filter, "tumor.mha")


######################## VTK ###############################
import vtk

colors = vtk.vtkNamedColors()

def visualize_tumor(path):

    # Loading base image
    brain = vtk.vtkMetaImageReader()
    brain.SetFileName(path)
    brain.Update()

    # Loading segmented tumor
    tumor = vtk.vtkMetaImageReader()
    tumor.SetFileName("tumor.mha")
    tumor.Update()


    # Marching cubes for visualisation
    bCubes = vtk.vtkMarchingCubes()
    bCubes.SetValue(0, 0.000001)
    bCubes.ComputeNormalsOn()
    bCubes.SetInputConnection(brain.GetOutputPort())

    tCubes = vtk.vtkMarchingCubes()
    tCubes.SetValue(0, 0.000001)
    tCubes.ComputeNormalsOn()
    tCubes.SetInputConnection(tumor.GetOutputPort())

    # Mappers
    bMapper = vtk.vtkPolyDataMapper()
    bMapper.ScalarVisibilityOff()
    bMapper.SetInputConnection(bCubes.GetOutputPort())

    tMapper = vtk.vtkPolyDataMapper()
    tMapper.ScalarVisibilityOff()
    tMapper.SetInputConnection(tCubes.GetOutputPort())

    # Actor set-up
    bActor = vtk.vtkActor()
    bActor.SetMapper(bMapper)
    bActor.GetProperty().SetOpacity(0.25)
    bActor.GetProperty().SetColor(colors.GetColor3d('DeepSkyBlue'))

    tActor = vtk.vtkActor()
    tActor.SetMapper(tMapper)
    tActor.GetProperty().SetOpacity(0.85)
    tActor.GetProperty().SetColor(colors.GetColor3d('FireBrick'))

    # Renderer set-up
    ren = vtk.vtkRenderer()
    ren.AddActor(bActor)
    ren.AddActor(tActor)
    ren.SetBackground(colors.GetColor3d("Black"))

    # Window set-up
    window = vtk.vtkRenderWindow()
    iren = vtk.vtkRenderWindowInteractor()
    window.SetInteractor(iren)
    window.AddRenderer(ren)

    # Let's get rolling
    window.Render()
    iren.Start()


# Actual main
if __name__ == '__main__':
    segment_tumor(sys.argv[1])
    visualize_tumor(sys.argv[1])
